<?php

namespace dsarhoya\DSYApiKeyAuthenticatorBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class dsarhoyaDSYApiKeyAuthenticatorExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        
        $container->setParameter('dsarhoya_dsy_api_key_authenticator.default_provider_user_class', 
                isset($config['default_provider_user_class']) ? $config['default_provider_user_class'] : null);
        
        $container->setParameter('dsarhoya_dsy_api_key_authenticator.api_key_entity', 
                isset($config['api_key_entity']) ? $config['api_key_entity'] : null);
        
        $container->setParameter('dsarhoya_dsy_api_key_authenticator.access_validation_strategy', 
                isset($config['access_validation_strategy']) ? $config['access_validation_strategy'] : null);
        
        $container->setParameter('dsarhoya_dsy_api_key_authenticator.error_codes', 
                isset($config['error_codes']) ? $config['error_codes'] : null);
        $container->setParameter('dsarhoya_dsy_api_key_authenticator.user_repository_method', isset($config['user_repository_method']) ? $config['user_repository_method'] : null);
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
    }
}
