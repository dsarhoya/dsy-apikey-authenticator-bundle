<?php

namespace dsarhoya\DSYApiKeyAuthenticatorBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('dsarhoya_dsy_api_key_authenticator');

        $rootNode
            ->children()
                ->scalarNode('default_provider_user_class')->end() //default_user_class
                ->arrayNode('api_key_entity')
                    ->children()
                        ->scalarNode('user_class')->isRequired()->end()
                        ->scalarNode('api_key_class')->isRequired()->end()
                    ->end()
                ->end()
                ->enumNode('access_validation_strategy')->values(array('simple', 'signature'))->defaultValue('simple')->end()
                ->arrayNode('error_codes')
                    ->useAttributeAsKey('class')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('code')->isRequired()->end()
                        ->end()
                    ->end()
                ->end()
                ->scalarNode('user_repository_method')->end()
            ->end();
        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.

        return $treeBuilder;
    }
}
