<?php

namespace dsarhoya\DSYApiKeyAuthenticatorBundle\Service;


/**
 * Description of ApiKeyService
 *
 * @author matias
 */
class ApiKeyService {
    
    /**
     * @param string $prefix
     * @return string
     */
    public function getRandomSecret($prefix){
        $secret = $prefix.(string)time().(string)rand(1, 1000);
        return md5($secret);
    }
}
