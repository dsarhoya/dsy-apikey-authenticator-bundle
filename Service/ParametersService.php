<?php

namespace dsarhoya\DSYApiKeyAuthenticatorBundle\Service;

/**
 * Description of ParametersService.
 */
class ParametersService
{
    public $defaultProviderUserClass;
    public $accessValidationStrategy;
    public $errorCodes;
    public $userRepositoryMethod;

    public function __construct($defaultProviderUserClass, $accessValidationStrategy, array $errorCodes, $userRepositoryMethod)
    {
        $this->defaultProviderUserClass = $defaultProviderUserClass;
        $this->accessValidationStrategy = $accessValidationStrategy;
        $this->errorCodes = $errorCodes;
        $this->userRepositoryMethod = $userRepositoryMethod;
    }
}
