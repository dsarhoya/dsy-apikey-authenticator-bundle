<?php

namespace dsarhoya\DSYApiKeyAuthenticatorBundle\Interfaces;

/**
 *
 * @author matias
 * //TODO: cambiar este nombre! UserApiKeyInterface
 */
interface EntityApiKeyInterface {
    public function getUser();
    public function getSecret();
    public function regenerate();
}
