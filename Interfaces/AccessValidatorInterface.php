<?php

namespace dsarhoya\DSYApiKeyAuthenticatorBundle\Interfaces;

interface AccessValidatorInterface {
    public function getCredentials(\Symfony\Component\HttpFoundation\Request $request);
    public function validateAccess($credentials, $user);
}
