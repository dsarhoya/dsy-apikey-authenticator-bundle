<?php

namespace dsarhoya\DSYApiKeyAuthenticatorBundle\Interfaces;

/**
 *
 * @author matias
 * //TODO: cambiar este nombre! UserApiKeyInterface
 */
interface ApiKeyInterface {
    public function getApiKey();
    public function isActive();
}
