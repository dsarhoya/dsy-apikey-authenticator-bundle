<?php

namespace dsarhoya\DSYApiKeyAuthenticatorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use DSYCredentials\ApiKeyAuthenticatorCredentials;

class EntityApiKeyExtensionsController extends Controller
{
    CONST USERS_LIMIT = 10;
    /**
     * @return \Doctrine\ORM\EntityRepository
     */
    private function getUserRepo(){
        $classes = $this->container->getParameter('dsarhoya_dsy_api_key_authenticator.api_key_entity');
        return $this->getDoctrine()->getRepository($classes['user_class']);
    }
    /**
     * @return \Doctrine\ORM\EntityRepository
     */
    private function getApiKeyRepo(){
        $classes = $this->container->getParameter('dsarhoya_dsy_api_key_authenticator.api_key_entity');
        return $this->getDoctrine()->getRepository($classes['api_key_class']);
    }
    
    public function indexAction(Request $request)
    {
        $repo = $this->getUserRepo();
        
        $page = $request->query->get('page', 1);
        $qb = $repo->createQueryBuilder('u');
        $qb->select('u');
        $qb->setFirstResult(($page-1)*self::USERS_LIMIT);
        $qb->setMaxResults(1);
        $limitUser = $qb->getQuery()->getOneOrNullResult();

        $qb = $repo->createQueryBuilder('u');
        $qb->select('u');
        if($limitUser){
            $qb->where('u.id >= :id');
            $qb->setParameter('id', $limitUser->getId());
            $qb->setMaxResults(self::USERS_LIMIT);
        }
        $users = $qb->getQuery()->getResult();
            
        $qb = $repo->createQueryBuilder('u');
        $qb->select('COUNT(u)');
        $count = $qb->getQuery()->getSingleScalarResult();
        
        return $this->render('dsarhoyaDSYApiKeyAuthenticatorBundle:EntityApiKeyExtensions:index.html.twig', array(
            'users'=>$users,
            'count'=>$count
        ));
    }
    
    public function getAction(Request $request, $userId)
    {
        $user = $this->getUserRepo()->find($userId);
        
        $signature = '';
        if($request->getMethod()=="POST"){
            $time = time();
            $credentials = new \DSYCredentials\ApiKeyCredentialsGenerator($request->request->get('path'), $user->getApiKeyLegacy()->getId(), $user->getApiKey(), null, array());
            $signature = sprintf('%s=%s&%s=%d&%s=%d',
                    \DSYCredentials\ApiKeyConstants::CREDENTIALS_KEY_SIGNATURE,
                    $credentials->getSignature(),
                    \DSYCredentials\ApiKeyConstants::CREDENTIALS_KEY_ID,
                    $credentials->getKey(),
                    \DSYCredentials\ApiKeyConstants::CREDENTIALS_KEY_TIMESTAMP,
                    $time
                    );

        }
        
        return $this->render('dsarhoyaDSYApiKeyAuthenticatorBundle:EntityApiKeyExtensions:get.html.twig', array(
            'user'=>$user, 
            'signature'=>$signature
            ));
    }
    
    public function generateAction(Request $request, $userId)
    {
        $classes = $this->container->getParameter('dsarhoya_dsy_api_key_authenticator.api_key_entity');
        $user = $this->getUserRepo()->find($userId);
        
        if(null === $key = $user->getApiKeyLegacy()){
            $key = new $classes['api_key_class'];
            if(!($key instanceof \dsarhoya\DSYApiKeyAuthenticatorBundle\Interfaces\EntityApiKeyInterface)){
                throw new \Exception(sprintf('La clase %s no implementa la interfaz EntityApiKeyInterface', $classes['api_key_class']));
            }
            $key->setUser($user);
            $this->getDoctrine()->getManager()->persist($key);
        }
        
        $key->regenerate();
        $this->getDoctrine()->getManager()->flush();
        
        return $this->redirect($this->generateUrl('entityApiKeysIndex'));
        
    }
}
