<?php

namespace dsarhoya\DSYApiKeyAuthenticatorBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

class ExtensionsController extends \dsarhoya\BaseBundle\Controller\BaseController
{
    CONST USERS_LIMIT = 10;
    
    public function indexAction(Request $request)
    {
        $repo = $this->getDoctrine()->getRepository($this->container->getParameter('dsarhoya_dsy_api_key_authenticator.default_provider_user_class'));
        
        $page = $request->query->get('page', 1);
        $qb = $repo->createQueryBuilder('u');
        $qb->select('u');
        $qb->setFirstResult(($page-1)*self::USERS_LIMIT);
        $qb->setMaxResults(1);
        $limitUser = $qb->getQuery()->getOneOrNullResult();

        $qb = $repo->createQueryBuilder('u');
        $qb->select('u');
        if($limitUser){
            $qb->where('u.id >= :id');
            $qb->setParameter('id', $limitUser->getId());
            $qb->setMaxResults(self::USERS_LIMIT);
        }
        $users = $qb->getQuery()->getResult();
            
        $qb = $repo->createQueryBuilder('u');
        $qb->select('COUNT(u)');
        $count = $qb->getQuery()->getSingleScalarResult();
        
        return $this->render('dsarhoyaDSYApiKeyAuthenticatorBundle:Extension:index.html.twig', array(
            'users'=>$users,
            'count'=>$count
        ));
    }
    
    public function getAction(Request $request, $userId)    
    {
        
        $repo = $this->getDoctrine()->getRepository($this->container->getParameter('dsarhoya_dsy_api_key_authenticator.default_provider_user_class'));
        $user = $repo->find($userId);
        
        $signature = '';
        if($request->getMethod()=="POST"){
            $time = time();
            $credentials = new \DSYCredentials\ApiKeyCredentialsGenerator($request->request->get('path'), $user->getId(), $user->getApiKey(), null, array());
            $signature = sprintf('%s=%s&%s=%d&%s=%d',
                    \DSYCredentials\ApiKeyConstants::CREDENTIALS_KEY_SIGNATURE,
                    $credentials->getSignature(),
                    \DSYCredentials\ApiKeyConstants::CREDENTIALS_KEY_ID,
                    $credentials->getKey(),
                    \DSYCredentials\ApiKeyConstants::CREDENTIALS_KEY_TIMESTAMP,
                    $time
                    );

        }
        
        return $this->render('dsarhoyaDSYApiKeyAuthenticatorBundle:Extension:get.html.twig', array(
            'user'=>$user,
            'signature'=>$signature
        ));
    }
    
    public function generateAction(Request $request, $userId)
    {
        $repo = $this->getDoctrine()->getRepository($this->container->getParameter('dsarhoya_dsy_api_key_authenticator.default_provider_user_class'));
        $user = $repo->find($userId);
        
        $user->setApikey($this->get('dsy.api_key_service')->getRandomSecret('prefijo-unico-del-proyecto'));
        $this->getDoctrine()->getManager()->flush();
        
        return $this->redirectToRoute('apiKeysIndex');
    }
}
