<?php

namespace dsarhoya\DSYApiKeyAuthenticatorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('dsarhoyaDSYApiKeyAuthenticatorBundle:Default:index.html.twig', array('name' => $name));
    }
}
