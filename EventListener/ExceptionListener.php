<?php

namespace dsarhoya\DSYApiKeyAuthenticatorBundle\EventListener;

use dsarhoya\DSYApiKeyAuthenticatorBundle\Error\BaseApiError;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

class ExceptionListener
{
    private $errorCodes;

    /**
     * @todo usar el parameters service.
     */
    public function __construct($errorCodes)
    {
        $this->errorCodes = $errorCodes;
    }

    public function onKernelException(ExceptionEvent $event)
    {
        // You get the exception object from the received event
        $exception = $event->getThrowable();

        if (!($exception instanceof BaseApiError)) {
            return;
        }
        $class = get_class($exception);
        $code = isset($this->errorCodes[$class]) ? $this->errorCodes[$class]['code'] : null;

        if (null !== $exception->getInternalCode()) {
            $code = $exception->getInternalCode();
        }
        $res = [
            'internal_code' => null === $code ? 0 : $code,
            'message' => $exception->getMessage(),
        ];

        if (null !== $exception->getInfo()) {
            $res['info'] = $exception->getInfo();
        }

        // Customize your response object to display the exception details
        $response = new Response();
        $response->setContent(json_encode($res));
        $response->headers->set('Content-Type', 'application/json');
        $response->setStatusCode($exception->getCode());

        // Send the modified response object to the event
        $event->setResponse($response);
    }
}
