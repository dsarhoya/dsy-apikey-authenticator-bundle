<?php

namespace dsarhoya\DSYApiKeyAuthenticatorBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Description of ClientRepository
 *
 * @author matias
 */
class ApiKeyRepository extends EntityRepository
{
    public function keys(){
        $qb = $this->createQueryBuilder('k');
        $qb->add('from', 'dsarhoyaDSYApiKeyAuthenticatorBundle:ApiKey k');
        $qb->leftJoin('k.user', 'u');
        $qb->leftJoin('u.company', 'c');
        $qb->addOrderBy('c.id', 'ASC');
        return $qb->getQuery()->getResult();
    }
    
    public function usersWithoutKey($userClass) {
        $qb = $this->createQueryBuilder('u');
        $qb->add('from', "$userClass u");
        $qb->leftJoin('u.company', 'c');
        $qb->leftJoin('u.apiKey', 'k');
        $qb->add('where', $qb->expr()->isNull('k.id'));
        $qb->addOrderBy('c.id', 'ASC');
        return $qb->getQuery()->getResult();
    }
}