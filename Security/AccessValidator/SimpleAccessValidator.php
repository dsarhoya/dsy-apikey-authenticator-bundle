<?php

namespace dsarhoya\DSYApiKeyAuthenticatorBundle\Security\AccessValidator;

use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use dsarhoya\DSYApiKeyAuthenticatorBundle\Interfaces\AccessValidatorInterface;

/**
 * Description of SimpleAccessValidator
 *
 * @author matias
 */
class SimpleAccessValidator implements AccessValidatorInterface{
    /**
     * 
     * @param \Symfony\Component\HttpFoundation\Request $request
     */
    public function getCredentials(\Symfony\Component\HttpFoundation\Request $request){
        
        $secret = false;
        if(!is_null($request->query->get('api-key'))){
            $secret = $request->query->get('api-key');
        }elseif (!is_null($request->request->get('api-key'))) {
            $secret = $request->request->get('api-key');
        }elseif (!is_null($request->headers->get('api-key'))) {
            $secret = $request->headers->get('api-key');
        }
        
        if (false === $secret) throw new BadCredentialsException('Invalid credentials');
        
        return $secret;
    }
    
    public function validateAccess($credentials, $user) {
        if ( $user instanceof \dsarhoya\DSYApiKeyAuthenticatorBundle\Interfaces\ApiKeyInterface){
            return $user->isActive();
        }
        return true;
    }
}
