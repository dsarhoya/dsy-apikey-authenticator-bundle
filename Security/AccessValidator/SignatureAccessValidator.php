<?php

namespace dsarhoya\DSYApiKeyAuthenticatorBundle\Security\AccessValidator;

use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use DSYCredentials\ApiKeyCredentialsValidator;
use dsarhoya\DSYApiKeyAuthenticatorBundle\Interfaces\AccessValidatorInterface;

/**
 * Description of SignatureAccessValidator
 *
 * @author matias
 */
class SignatureAccessValidator implements AccessValidatorInterface{
    /**
     * 
     * @param \Symfony\Component\HttpFoundation\Request $request
     */
    public function getCredentials(\Symfony\Component\HttpFoundation\Request $request){
        
        $url_string = $request->getPathInfo();
        
        if (null !== $qs = $request->getQueryString()) {
            $url_string .= '?'.$qs;
        }
        if (!$request->query->get('key_id')) throw new BadCredentialsException('Invalid credentials');
        
        return array('key_id'=>$request->query->get('key_id'), 'url_string'=>$url_string, 'request_params'=>$request->request->all());
    }
    
    public function validateAccess($credentials, $user) {
        $credentialsValidator = new ApiKeyCredentialsValidator($credentials['url_string'], $credentials['request_params']);
        
        //Acá podría preguntar por la llave relacionada si fuese así?
        $credentialsValidator->setKeyIdAndSecret($credentials['key_id'], $user->getApiKey());
        
        return $credentialsValidator->areValid();
    }
}
