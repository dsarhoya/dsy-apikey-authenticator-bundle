<?php

namespace dsarhoya\DSYApiKeyAuthenticatorBundle\Security\Simple;

use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

/**
 * Description of ApiKeyUserProvider
 *
 * @author matias
 */
class ApiKeyUserProvider implements UserProviderInterface
{
    /**
     *
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    private $user_class;
    private $access_validation_strategy;
    private $user_repository_method;
    
    public function __construct($entity_manager, $user_class, $access_validation_strategy, $user_repository_method) {
        $this->em       = $entity_manager;
        $this->user_class  = $user_class;
        $this->access_validation_strategy = $access_validation_strategy;
        $this->user_repository_method = $user_repository_method;
    }
    
    public function getUsernameForCredentials($credentials){
        
        $repo = $this->em->getRepository($this->user_class);
        //Se asume que todos los usuarios tienen campo apiKey. Esto se podría configurar.
        
        $key = null;
        
        if($this->access_validation_strategy === 'simple'){
            $method = $this->user_repository_method !== null ? $this->user_repository_method : 'findOneByApiKey';
            $key = $repo->$method($credentials);
        }else{
            $key = $repo->find($credentials['key_id']);
        }
        
        if(!$key) return null;
        
        //Se asume que todos los usuarios tienen un id
        $username = $key->getId();

        return $username;
    }

    public function loadUserByUsername($username)
    {
        $repo = $this->em->getRepository($this->user_class);
        //Se asume que el username es el id
        $user = $repo->find($username);
        
        if(!$user) return null;
        
        return $user;
    }

    public function refreshUser(UserInterface $user)
    {
        // this is used for storing authentication in the session
        // but in this example, the token is sent in each request,
        // so authentication can be stateless. Throwing this exception
        // is proper to make things stateless
        throw new UnsupportedUserException();
    }

    public function supportsClass($class)
    {
        return $this->user_class === $class;
    }
}