<?php

namespace dsarhoya\DSYApiKeyAuthenticatorBundle\Security\Guard;

use dsarhoya\DSYApiKeyAuthenticatorBundle\Service\ParametersService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

class BaseApiKeyAuthenticator extends AbstractGuardAuthenticator
{
    private $parametersService;

    /**
     * @var \dsarhoya\DSYApiKeyAuthenticatorBundle\Interfaces\AccessValidatorInterface
     */
    private $accessValidator;

    public function __construct(ParametersService $parametersService)
    {
        $this->parametersService = $parametersService;

        if ('simple' === $this->parametersService->accessValidationStrategy) {
            $this->accessValidator = new \dsarhoya\DSYApiKeyAuthenticatorBundle\Security\AccessValidator\SimpleAccessValidator();
        } else {
            $this->accessValidator = new \dsarhoya\DSYApiKeyAuthenticatorBundle\Security\AccessValidator\SignatureAccessValidator();
        }
    }

    /**
     * Called on every request to decide if this authenticator should be
     * used for the request. Returning false will cause this authenticator
     * to be skipped.
     */
    public function supports(Request $request)
    {
        if (!is_null($request->query->get('api-key')) ||
            !is_null($request->request->get('api-key')) ||
            !is_null($request->headers->get('api-key'))) {
            return true;
        }

        return false;
    }

    /**
     * Called on every request. Return whatever credentials you want to
     * be passed to getUser() as $credentials.
     */
    public function getCredentials(Request $request)
    {
        $credentials = $this->accessValidator->getCredentials($request);

        return $credentials;
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        if (!$credentials) {
            throw new BadCredentialsException('API Key Inválida');
        }

        $username = $userProvider->getUsernameForCredentials($credentials);

        if (!$username) {
            throw new BadCredentialsException('API Key Inválida');
        }

        $user = $userProvider->loadUserByUsername($username);

        if (!$user) {
            throw new BadCredentialsException('API Key Inválida');
        }

        return $user;
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        
        // check credentials - e.g. make sure the password is valid
        // no credential check is needed in this case

        // return true to cause authentication success
        return $this->accessValidator->validateAccess($credentials, $user);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        // on success, let the request continue
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        // dd($exception);
        $data = [
            'message' => strtr($exception->getMessageKey(), $exception->getMessageData()),

            // or to translate this message
            // $this->translator->trans($exception->getMessageKey(), $exception->getMessageData())
        ];

        return new JsonResponse($data, Response::HTTP_FORBIDDEN);
    }

    /**
     * Called when authentication is needed, but it's not sent.
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        $data = [
            // you might translate this message
            'message' => 'Authentication Required',
        ];

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }

    public function supportsRememberMe()
    {
        return false;
    }
}
