<?php

namespace dsarhoya\DSYApiKeyAuthenticatorBundle\Security\ApiKeyEntity;

use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

/**
 * Description of ApiKeyUserProvider
 *
 * @author matias
 */
class ApiKeyUserProvider implements UserProviderInterface
{
    private $em;
    private $api_key_entity;
    private $access_validation_strategy;
    
    public function __construct($entity_manager, $api_key_entity, $access_validation_strategy) {
        $this->em       = $entity_manager;
        $this->api_key_entity  = $api_key_entity;
        $this->access_validation_strategy = $access_validation_strategy;
    }
    
    /*
     * TODO: hay que obligar a implementar este método. Se podría (obligar?) extender de un provider base.
     */
    public function getUsernameForCredentials($credentials)
    {
        $repo = $this->em->getRepository($this->api_key_entity['api_key_class']);
        //Se asume que todos los usuarios tienen campo apiKey. Esto se podría configurar.
        
        $key = null;
        
        if($this->access_validation_strategy === 'simple'){
            $key = $repo->findOneBy(array(
                'secret'=>$credentials
            ));
        }else{
            $key = $repo->find($credentials['key_id']);
        }
        
        if(!$key) return null;
        
        $username = $key->getUser()->getId();

        return $username;
    }

    public function loadUserByUsername($username)
    {
        $repo = $this->em->getRepository($this->api_key_entity['user_class']);
        //Se asume que el username es el id
        $user = $repo->find($username);
        
        if(!$user) return null;
        
        return $user;
    }

    public function refreshUser(UserInterface $user)
    {
        // this is used for storing authentication in the session
        // but in this example, the token is sent in each request,
        // so authentication can be stateless. Throwing this exception
        // is proper to make things stateless
        throw new UnsupportedUserException();
    }

    public function supportsClass($class)
    {
        return $this->user_class === $class;
    }
}