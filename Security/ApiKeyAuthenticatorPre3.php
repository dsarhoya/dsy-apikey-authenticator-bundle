<?php

namespace dsarhoya\DSYApiKeyAuthenticatorBundle\Security;

use Symfony\Component\Security\Core\Authentication\SimplePreAuthenticatorInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use dsarhoya\DSYApiKeyAuthenticatorBundle\Security\ApiKeyAuthenticatorBase;

/**
 * Description of ApiKeyAuthenticator
 * TODO: Simpl es mal nombre, debería reflejar que la llave está en usuario -> debiéra ser User
 * TODO: probablemente, este y el otro se pueden mover a uno solo, que extienda de una base. Vale la pena?
 * @author matias
 */
class ApiKeyAuthenticatorPre3 extends ApiKeyAuthenticatorBase implements SimplePreAuthenticatorInterface, AuthenticationFailureHandlerInterface
{
    
}