<?php

namespace dsarhoya\DSYApiKeyAuthenticatorBundle\Security\Provider;

use Symfony\Component\Security\Core\User\UserProviderInterface;

interface BaseApiKeyUserProviderInterface extends UserProviderInterface
{
}
