<?php

namespace dsarhoya\DSYApiKeyAuthenticatorBundle\Security\Provider;

use Doctrine\ORM\EntityManagerInterface;
use dsarhoya\DSYApiKeyAuthenticatorBundle\Service\ParametersService;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\UserInterface;

class BaseApiKeyUserProvider implements BaseApiKeyUserProviderInterface
{
    protected $em;
    protected $parametersSrv;

    public function __construct(EntityManagerInterface $em, ParametersService $parametersSrv)
    {
        $this->em = $em;
        $this->parametersSrv = $parametersSrv;
    }

    public function loadUserByUsername($username)
    {
        $repo = $this->em->getRepository($this->parametersSrv->defaultProviderUserClass);
        //Se asume que el username es el id
        $user = $repo->find($username);

        if (!$user) {
            return null;
        }

        return $user;
    }

    public function refreshUser(UserInterface $user)
    {
        // this is used for storing authentication in the session
        // but in this example, the token is sent in each request,
        // so authentication can be stateless. Throwing this exception
        // is proper to make things stateless
        throw new UnsupportedUserException();
    }

    public function supportsClass($class)
    {
        return $this->parametersSrv->defaultProviderUserClass === $class;
    }

    /**
     * TODO: Pregunta: ¿Podemos prescindir del userRepositoryMethod ahora que podemos sobreescribir esto?
     *
     * @param [type] $credentials
     *
     * @return void
     */
    public function getUsernameForCredentials($credentials)
    {
        $repo = $this->em->getRepository($this->parametersSrv->defaultProviderUserClass);
        //Se asume que todos los usuarios tienen campo apiKey. Esto se podría configurar.

        $key = null;

        if ('simple' === $this->parametersSrv->accessValidationStrategy) {
            $method = null !== $this->parametersSrv->userRepositoryMethod ? $this->parametersSrv->userRepositoryMethod : 'findOneByApiKey';
            $key = $repo->$method($credentials);
        } else {
            $key = $repo->find($credentials['key_id']);
        }

        if (!$key) {
            return null;
        }

        //Se asume que todos los usuarios tienen un id
        $username = $key->getId();

        return $username;
    }
}
