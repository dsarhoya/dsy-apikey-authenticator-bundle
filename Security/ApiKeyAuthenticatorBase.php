<?php

namespace dsarhoya\DSYApiKeyAuthenticatorBundle\Security;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Authentication\Token\PreAuthenticatedToken;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Description of ApiKeyAuthenticator
 * TODO: Simpl es mal nombre, debería reflejar que la llave está en usuario -> debiéra ser User
 * TODO: probablemente, este y el otro se pueden mover a uno solo, que extienda de una base. Vale la pena?
 * @author matias
 */
class ApiKeyAuthenticatorBase
{
    private $em;
    private $access_validation_strategy;
    
    /**
     *
     * @var \dsarhoya\DSYApiKeyAuthenticatorBundle\Interfaces\AccessValidatorInterface
     */
    private $accessValidator;
    
    public function __construct($entity_manager, $access_validation_strategy) {
        $this->em       = $entity_manager;
        $this->access_validation_strategy = $access_validation_strategy;
        if($this->access_validation_strategy === 'simple'){
            $this->accessValidator = new \dsarhoya\DSYApiKeyAuthenticatorBundle\Security\AccessValidator\SimpleAccessValidator();
        }else{
            $this->accessValidator = new \dsarhoya\DSYApiKeyAuthenticatorBundle\Security\AccessValidator\SignatureAccessValidator();
        }
    }

    public function createToken(Request $request, $providerKey)
    {
        
        $credentials = $this->accessValidator->getCredentials($request);
        
        //Hacer el token con toda la información que se necesite. En la autentificación se toma toda esa info se tiene que hacer el cálculo.
        return new PreAuthenticatedToken(
            'anon.',
            $credentials,
            $providerKey
        );
    }

    public function authenticateToken(TokenInterface $token, UserProviderInterface $userProvider, $providerKey)
    {
        $credentials = $token->getCredentials();
        
        $username = $userProvider->getUsernameForCredentials($credentials);
        
        if(!$username){
            throw new BadCredentialsException('Incorrect username or password');
        }
        
        $user = $userProvider->loadUserByUsername($username);
        
        if(!$user){
            throw new BadCredentialsException('Incorrect username or password');
        }
        if(true !== $this->accessValidator->validateAccess($credentials, $user)){
            throw new BadCredentialsException('Incorrect username or password');
        }
        
        return new PreAuthenticatedToken(
            $user,
            $credentials,
            $providerKey,
            $user->getRoles()
        );
    }

    public function supportsToken(TokenInterface $token, $providerKey)
    {
        return $token instanceof PreAuthenticatedToken && $token->getProviderKey() === $providerKey;
    }
    
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        return new Response("Authentication Failed!", 403);
    }
}