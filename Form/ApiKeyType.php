<?php

namespace dsarhoya\DSYApiKeyAuthenticatorBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ApiKeyType extends AbstractType
{
    protected $user_class;
    
    public function __construct($classes) {
        $this->user_class = $classes['user']['class'];
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user', 'entity', array(
                'property'=>'descriptiveName',
                'class'=>  $this->user_class,
                'choices'=>$options['choices'],
                'label'=>'Usuario'
            ))
        ;
        $builder
            ->add('submit', 'submit', array(
                'label'=>'Crear',
                'attr'=>array(
                    'class'=>'btn btn-primary'
                )
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'dsarhoya\DSYApiKeyAuthenticatorBundle\Entity\ApiKey',
            'choices'=>array()
        ));
    }

    public function getName()
    {
        return 'dsy_api_key_type';
    }
}
